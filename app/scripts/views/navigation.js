/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var NavigationView = Backbone.View.extend({
        template: JST['app/scripts/templates/navigation.ejs'],
        el: '.navigation',
        events: {
            'click .add-gallery' : 'addGallery'
        },

        initialize: function (options) {
            this.parent = options.parent;
        },

        addGallery: function(e) {
            e.preventDefault();
            var tag = this.$el.find('input').val();
            this.parent.trigger("gallery:add", tag);
        },

        render: function () {
            this.$el.html(this.template());
        }
    });

    return NavigationView;
});
