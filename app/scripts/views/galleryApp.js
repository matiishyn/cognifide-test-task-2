/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    '../models/gallery',
    'views/tagGallery',
    'views/navigation'
], function ($, _, Backbone, JST, Gallery, TagGalleryView, NavigationView) {
    'use strict';

    var GalleryView = Backbone.View.extend({
        el: '.container',
        template: JST['app/scripts/templates/galleryApp.ejs'],

        tagName: 'div',

        id: '',

        className: '',

        events: {},

        initialize: function () {
            this.render();
            // render nav
            var nav = new NavigationView({parent: this});
            nav.render();

            // create gallery html
            this.createGallery('html');

            // create gallery css
            this.createGallery('css');

            this.addHandlers();
        },

        addHandlers: function() {
            this.on("gallery:add", this.createGallery, this);
        },

        createGallery: function(tag) {
            var galleryModel = new Gallery();
            var galleryView = new TagGalleryView({model: galleryModel, tag: tag});
            this.$el.find('.galleries').append(galleryView.$el);
        },

        render: function () {
            this.$el.html(this.template());
        }
    });

    return GalleryView;
});
