/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/tagGallery',
    'views/galleryImage'
], function ($, _, Backbone, JST, TagGalleryCollection, GalleryImageView) {
    'use strict';

    var TaggalleryView = Backbone.View.extend({
        template: JST['app/scripts/templates/tagGallery.ejs'],
        events: {
            'click .remove-gallery': 'removeGallery',
            'click .sort-date-published': 'sort',
            'click .sort-date-taken': 'sort'
        },

        removeGallery: function(e) {
            e.preventDefault();
            if(confirm('Do you really want to delete "' + this.model.toJSON().title + '"?')){
                this.remove(); // remove view
            }
        },

        sort: function(e) {
            e.preventDefault();
            this.collection.comparator = this.$(e.target).data('sort');
            this.collection.sort();
            this.renderCollection();
        },

        initialize: function (options) {
            this.tag = options.tag;
            this.model = options.model;
            this.model.on('change', this.parseReceivedData, this);
            this.fetchModel();

            this.on("render:post", this.onRender.bind(this));
        },

        onRender:function() {
            this.collection.on('reset', this.renderCollection, this);
        },

        fetchModel: function() {
            this.model.fetch({
                dataType: 'jsonp',
                jsonp: 'jsoncallback',
                data: {
                    format: 'json',
                    tags: this.tag
                }
            });
        },

        parseReceivedData: function(model) {
            var fourItems = this.reduceArray(model.toJSON().items, 4);
            this.collection = new TagGalleryCollection(fourItems);
            this.render();
        },
        /**
         * we receive 20 items, but for app we will need only four of them
         * @param array {array}
         * @param number {integer} - number of first items that will remain
         */
        reduceArray: function(array, number) {
            return _.filter(array, function(item, index) {return index < number});
        },

        render: function () {
            this.$el.append(this.template(this.model.toJSON()));
            this.renderCollection();
            this.trigger("render:post");
        },

        renderCollection: function() {
            this.$el.find('.tag-images').empty();
            this.collection.each(this.renderOneImage.bind(this));
        },

        renderOneImage: function(model, index, collection) {
            var imageView = new GalleryImageView({model: model});
            this.$el.find('.tag-images').append(imageView.render().$el);
        }
    });

    return TaggalleryView;
});
