/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var GalleryimageView = Backbone.View.extend({
        template: JST['app/scripts/templates/galleryImage.ejs'],

        tagName: 'div',

        id: '',

        className: '',

        events: {},

        initialize: function () {

        },

        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    return GalleryimageView;
});
