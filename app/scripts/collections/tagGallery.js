/*global define*/

define([
    'underscore',
    'backbone',
    'models/gallery'
], function (_, Backbone, TaggalleryModel) {
    'use strict';

    var TaggalleryCollection = Backbone.Collection.extend({

    });

    return TaggalleryCollection;
});
