/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var GalleryModel = Backbone.Model.extend({
        url: 'https://api.flickr.com/services/feeds/photos_public.gne',

        initialize: function() {
        },

        defaults: {
        },

        validate: function(attrs, options) {
        },

        parse: function(response, options)  {
            return response;
        }
    });

    return GalleryModel;
});
