/*global require*/
'use strict';

require.config({
    shim: {

    },
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        bootstrap: '../bower_components/bootstrap/dist/js/bootstrap',
        'requirejs-text': '../bower_components/requirejs-text/text',
        requirejs: '../bower_components/requirejs/require',
        modernizr: '../bower_components/modernizr/modernizr',
        lodash: '../bower_components/lodash/dist/lodash.compat'
    }
});

require([
    'backbone',
    'views/galleryApp'
], function (Backbone, Gallery) {
    Backbone.history.start();

    new Gallery();
});
